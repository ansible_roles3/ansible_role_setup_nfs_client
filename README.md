Ansible_role_setup_nfs_client
=========

Роль позволяет равернуть и настроить клиентскую часть NFS.

Requirements
------------
```
collection:
  - ansible.posix.mount
```
You might already have this collection installed if you are using the ansible package. It is not included in ansible-core. To check whether it is installed, run ```ansible-galaxy collection list```.
To install it, use: ```ansible-galaxy collection install ansible.posix```.

Role Variables
--------------
```
share_dir: "/mnt/share_directory" # Shared directory on the NFS client
ip_nfs_server: "192.168.60.201" # IP address of the NFS server
nfs_server_share_dir: "/mnt/share_directory" # The path to the shared directory on the NFS server

#Determines if the filesystem should be mounted on boot.
#Only applies to Solaris and Linux systems.
#For Solaris systems, 'true' will set 'yes' as the value of mount at boot in /etc/vfstab.
#For Linux, FreeBSD, NetBSD and OpenBSD systems, 'false' will add 'noauto' to mount options in /etc/fstab.
#To avoid mount option conflicts, if 'noauto' specified in opts, mount module will ignore boot.
#This parameter is ignored when state is set to 'ephemeral'.
boot_mount: "false"
```
Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
    - hosts: nfs-clients
      roles:
         - ansible_role_setup_nfs_client
```

License
-------

BSD

Author Information
------------------
AlZa
